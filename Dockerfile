FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    software-properties-common && \
    apt-add-repository ppa:seafile/seafile-client && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    seafile-cli \
    oathtool && \
    rm -rf /var/lib/apt/lists/*

ADD docker-entrypoint.sh /docker-entrypoint.sh

RUN addgroup --system --gid 1000 seafile-sync && \
    adduser --system --ingroup seafile-sync --uid 1000 --shell /bin/sh --home /config seafile-sync

USER seafile-sync

ENV TASK="sync"    
ENV LIBRARY=""
ENV SERVER=""
ENV EMAIL=""
ENV PASSWORD=""
ENV OTP_TOKEN=""

VOLUME /sync

ENTRYPOINT ["/docker-entrypoint.sh"]
