#!/usr/bin/env bash
set -Eeo pipefail

trap "exit" INT

mkdir -p ~/seafile
seaf-cli init -d ~/seafile
seaf-cli start

until [ -S ~/seafile/seafile-data/seafile.sock ]; do
  sleep 1;
done

if [ -z "$OTP_TOKEN" ]; then
  echo "syncing library"
  seaf-cli "$TASK" -l "$LIBRARY" -s "$SERVER" -d "/sync" -u "$EMAIL" -p "$PASSWORD"
else
  echo "syncing library with OTP"
  seaf-cli "$TASK" -l "$LIBRARY" -s "$SERVER" -d "/sync" -u "$EMAIL" -p "$PASSWORD" --tfa $(oathtool --totp -b "$OTP_TOKEN")
fi

until seaf-cli status | grep synchronized; do
  seaf-cli status
  sleep 5
done

seaf-cli stop
